/* Hannah Dicus
hdicus
Lab #5
Section 002
Lab TA: Hanjie (Hollis) Liu
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <array>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/

   /* Create struct array to hold deck */
   Card deck_array[52];
   /* First for loop corresponds to suits */
   for(int x = 0; x < 4; x++) {
     /* Second for loop corresponds to card values */
     for(int val = 2; val < 15; val++) {
       deck_array[(x*12)+(val-2)].suit = static_cast <Suit>(x);
       deck_array[(x*12)+(val-2)].value = val;            
     }
   }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

    /*Initialize pointers used as parameters for random_shuffle */

    /* Pointer to beginning of array */
    Card *para1 = &deck_array[0];
    /* Pointer to 1 past the end of the array */
    Card *para2 = &deck_array[0] + 52;
    /* Pointer to the function */
    int (*mrPtr)(int) = &myrandom;
   
    /* Call function */
    random_shuffle(para1, para2, mrPtr);    

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
   
   /* Create struct array to hold hand */
   Card hand[5]; 
   /* Set deck equal to the first five card in the newly shuffled array */
   for(int y = 0; y < 5; y++) {
     hand[y] = deck_array[y];
   }

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
    
    /* Declare bool variable to hold return value from suit_order function */
    bool var;
    /* Declare int varaible to hold temporary values when swapping */
    Card temp;  

    /* Use loop to sort the hand */
    for(int i = 0; i <= 3; i++) {
      /* Call suit_order function */
      var = suit_order(hand[i],hand[i+1]); 
      /* If lhs < rhs, swap */
      if(var == 1) {
        temp = hand[i];
        hand[i] = hand[i+1];
        hand[i+1] = temp;
      }
    }
    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
    
    /* Declare arrays of strings to hold data */
    string suitName[5];
    string valueNum[5];

    /* Use for loop to implement through the hand */
    for(int j = 0; j < 5; j++) {
      /* Call get_card_name */
      suitName[j] = get_card_name(hand[j]);
      /* Call get_suit_code */ 
      valueNum[j] = get_suit_code(hand[j]);
    }

    /* Print hand */
    for(int k = 0; k < 5; k++) {
      cout << setw(10) << right << suitName[k] << " " << valueNum[k] << endl;
    }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
   /* Declare bool variable */
   bool order;
   /* Check to see if lhs < rhs */
   if(&lhs < &rhs) {
     order = true;
   }

   else {
     order = false;
   }
  
   /* return whether lhs is larger than rhs or not */
   return(order);
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  switch (c.value) {
    case 1:    return "1 of";
    case 2:    return "2 of";
    case 3:    return "3 of";
    case 4:    return "4 of";
    case 5:    return "5 of";
    case 6:    return "6 of";
    case 7:    return "7 of";
    case 8:    return "8 of";
    case 9:    return "9 of";
    case 10:   return "10 of";
    case 11:   return "Jack of";
    case 12:   return "Queen of";
    case 13:   return "King of";
    case 14:   return "Ace of";
    default:   return "";
  }
}
  
